function wrix --description "Scale and convert images to various sizes and formats."
	# This makes wildcards matching filenames containing spaces safer -- this
	# script will still break if any filenames contain tab or newline or other
	# control characters.

	# Before anything else, if gm isn't installed, print an error message.
	if test ! ( which gm )
		echo "GraphicsMagick is not installed, or the gm executable is not in your PATH."
		echo "You need to properly install GraphicsMagick before running WRIX."
		echo "<http://www.graphicsmagick.org/README.html>"
		return 1
	end

	switch "$argv[ 1 ]"
		# If there are no parameters, or if the first is -h or --help, print usage
		# information.
		case "" "-h" "--help"
			echo "Run WRIX in interactive mode. This is the best option if you aren't"
			echo "very comfortable with the commandline."
			echo \n"  wrix -i"\n

			echo "Other run modes and features are forthcoming."
			echo "For more information, see the README.md file that came with WRIX,"
			echo "or visit <https://github.com/terrycloth/wrix>"

			return

		# Else if the first parameter is -i, walk user through some prompts to gather
		# information about the current job.
		case "-i"
			echo \n"Hello! When using WRIX, you start with a high resolution image, and"
			echo "export to different sizes to allow for more responsive web design when"
			echo "you post your work online. So, if the original file were called"
			echo "picture.png, you might export picture-uhd.jpg, picture-hd.jpg, and"
			echo "picture-sd.jpg for Ultra HD, HD, and Standard Definition displays."
			echo "You can use just about any standard image file, as well as GIMP"
			echo "and Krita work files, and Open Raster Archives."

			echo \n"Hit Ctrl-C at any time to cancel this."

			echo \n"** Input images:"
			echo "First, what image or images are we exporting? You can enter a"
			echo "single complete filename, or do a “fuzzy” match of several files"
			echo "using the * and ? wildcards, where ? matches any single character,"
			echo "and * matches any number of characters. For example, p??k will match"
			echo "puck, pack, park, and so on. And typing something like *.png will"
			echo "grab all the png files in the current directory. If you need to use"
			echo "more than one pattern or exact filename, run the script again."
			echo "...And don't worry, the original files won't be touched.)"
			read --delimiter='\n\t'  input
			set input_files ( find . -maxdepth 1 -iname $input )

			echo \n"** Batch size:"
			echo "How many different sizes would you like to create for each image?"
			read --delimiter='\n\t'  batch_number

			echo \n"** Suffixes:"
			set i 1
			while test $i -le $batch_number
				echo "#$i - what suffix should we append to the base filename?"
				read --delimiter='\n\t'  suffix
				set suffixes $suffixes "$suffix"
				set i ( math "$i + 1" )
			end

			for suffix in $suffixes
				echo \n"** Output image size:"
				echo "  $suffix: What size should the exported image be?"
				echo "You can type a percentage (e.g., 50%), and it will evenly scale the"
				echo "image by that proportion. Or you can set a specific WIDTHxHEIGHT."
				echo "By default, this preserves the original aspect ratio, so 500x500"
				echo "will make sure that both the width or height are 500 pixels long"
				echo "maximum. To force the exact dimensions you specify, add an"
				echo "exclamation point (e.g., for a thumbnail, you might use 100x100!)."
				echo "You can also specify just the width or height, by leaving out one"
				echo "of the numbers but keeping the x. A single number without an x sets"
				echo "the length for the longest side, keeping the original aspect ratio."
				read --delimiter='\n\t'  scale
				set resizes $resizes "$scale"
			end

			echo \n"** Color depth:"
			echo "What color depth do you want to use? Enter the number of bits per"
			echo "color channel. 8 and 16 are the most common. Higher numbers allow"
			echo "more shades of color, which is important for gradients and digital"
			echo "paintings to look good, but also makes for a larger file size."
			echo "There's also not much point in using a greater color depth than you"
			echo "need: any JPEG images that you output, and any work files from"
			echo "GIMP 2.8 and earlier can't go higher than 8-bit colors. Images with"
			echo "very few, flat colors may be able to go even lower than 8 to save"
			echo "file size."
			read --delimiter='\n\t'  color_depth

			echo \n"** Output format:"
			echo "What image format should the exported images use? (Note that this"
			echo "will become the exported file extension, though you don't have to"
			echo "include the dot.)"
			read --delimiter='\n\t'  format

			switch "$format"
				# JPEG and JNG are lossy formats that support chroma subsampling.
				case "jpg" "jpeg" "JPG" "JPEG" "jng" "JNG"
					set format_is_lossy true
					set format_supports_chroma true

				# JPEG 2000 and WebP are (potentially) lossy, but don't support chroma
				# subsampling.
				case "jp2" "JP2" "webp" "WEBP"
					set format_is_lossy true
					set format_supports_chroma false

				# All other formats are lossless compression.
				case "*"
					set format_is_lossy false
					set format_supports_chroma false
			end

			# Get quality settings for lossy formats.
			if test "$format_is_lossy" = "true"
				echo \n"** Image quality:"
				echo "For lossy image formats, the compression/quality setting chooses how"
				echo "to encode the image: there's a trade-off between good picture"
				echo "quality and small file size. 0 is the lowest quality and smallest"
				echo "file, 100 is the highest quality and largest file. For JPEGs, I"
				echo "would normally set it around 85, but you can try this with"
				echo "different settings and see how small you can go while still having"
				echo "an acceptable quality image."
				read --delimiter='\n\t'  quality
				set quality "-quality" "$quality"

				# Also get chroma subsampling ratio for supported formats.
				if test "$format_supports_chroma" = "true"
					echo \n"** Chroma subsampling:"
					echo "Another way JPEGs images can save space is by converting the color"
					echo "encoding from the usual red-green-blue values to luminance versus"
					echo "chrominance; i.e., it can save the brightness data, but cut back on"
					echo "the color data. A ratio of 4:4:4 means no subsampling, and therefore"
					echo "no loss of color data, while 4:2:2 and 4:1:1 and so on represent"
					echo "progressively greater cuts in chrominance, reducing file size."
					echo "Again, you can try it different ways to see how small you can go"
					echo "while maintaining acceptable image quality."
					echo "If in doubt, though, it's safe to stick with 4:4:4"
					read --delimiter='\n\t'  subsampling
					set subsampling "-sampling-factor" "$subsampling"

				# Otherwise, chroma subsampling doesn't matter, but can't be an empty
				# string, so...
				else
					set subsampling "-sampling-factor" "4:4:4"
				end

			# For lossless formats, "quality" actually sets the compression rate, with 95
			# being the highest compression, because reasons. See the GM documentation:
			# <http://www.graphicsmagick.org/GraphicsMagick.html#details-quality>.
			# Chroma subsampling doesn't matter for these formats either, but can't be
			# left as empty strings, so...
			else
				set quality "-quality" 95
				set subsampling "-sampling-factor" "4:4:4"
			end


			# Generate output file *basenames*.
			for file in $input_files
				set output_basenames $output_basenames ( echo "$file" | sed -r 's/\.\/(.*)\.\w+$/\1/' )
			end
			# Generate output final output filenames.
			for file in $output_basenames
				set i 1
				while test $i -le ( count $suffixes )
					set output_files $output_files "$file$suffixes[ $i ].$format"
					set i ( math "$i + 1" )
				end
			end

			echo \n"** Final check:"
			echo "Okay, so here's what the batch exports look like:"
			set i 1
			for in_file in $input_files
				echo "$in_file →"
					set j 1
					while test $j -le ( count $suffixes )
						echo "  $output_files[ $i ] - scaled to $resizes[ $j ]"
						set i ( math "$i + 1" )
						set j ( math "$j + 1" )
					end
			end
			echo "All images are using $color_depth-bit-per-channel color."
			if test "$format_is_lossy" = "true"
				echo "All images at $quality[ 2 ]% quality."
				if test "$format_supports_chroma" = "true"
					echo "All images using $subsampling[ 2 ] chroma subsampling."
				end
			else
				echo "Lossless $format images will use the highest compression."
			end

			while true
				echo \n"Does this batch look right? (yes/no)"
				read --delimiter='\n\t'  check_batch
				switch "$check_batch"
					case "n" "N" "no" "No" "NO" "nope" "Nope" "NOPE" "nah" "Nah" "NAH"
						echo \n"Okay, that's fine. Let's start over."
						echo "(If you keep finding a problem here, check the README file, or"
						echo "ask about it on GitHub - <https://github.com/terrycloth/wrix>.)"
						return 2
					case "y" "Y" "yes" "Yes" "YES" "yeah" "Yeah" "YEAH" "yup" "Yup" "YUP"
						# if "yes", break out and start GraphicsMagick.
						break
					case "*"
						echo "What was that? Try again."
				end
			end

		# Else if the first parameter is -d, walk user through the same prompts, but
		# save settings to environment variables (set -U).
		case "-d"
			# Maybe use slightly different messages? Probably a user will be familiar
			# with how WRIX works if they want to set defaults.

		case "--"
			# Else if there's more than one parameter, but the first is the double dash,
			# use default *resize* settings on the specified input files and output
			# format.

			# Else if there is only one parameter, and it's the double dash, use all
			# default settings.

		# Otherwise, extract all settings from commandline parameters.
		case "*"
			# Keep track of where we are in the process: A double dash switches from
			# collecting resize information to collecting input files and output format.

				# Last parameter is the output format.
				# Put rest of parameters in list of images to process.
	end


	# Okay, now that we have the input/output files figured out, we can finally
	# process them. Send the image to GraphicsMagick for processing.
	echo \n"All systems go!"
	# $i will track progress through the list of output files.
	# $j will track progress through the list of input files.
	# $k will track progress through the list of resizes.
	set i 1
	set j 1
	set k 1
	set total_output ( count $output_files )
	while test $i -le $total_output
		echo "$i/$total_output: Exporting $output_files[ $i ]..."

		# If the input file is a GIMP, Krita, ORA, or SVG file, export to PNG
		# first and work from the PNG.
		if test ( echo "$input_files[ $j ]" | grep -iEe '\.kra$' )
			krita "$input_files[ $j ]" --export --export-filename "$input_files[ $j ].png"
		else if test ( echo "$input_files[ $j ]" | grep -iEe '\.xcf(\.)?(bz2)?$' )
			gimp -i --batch-interpreter "python-fu-eval" \
				-b 'image = pdb.gimp_file_load( "'$input_files[ $j ]'", "'$input_files[ $j ]'" ); pdb.gimp_image_flatten( image ); drawable = pdb.gimp_image_get_active_drawable( image ); pdb.file_png_save_defaults( image, drawable, "'$input_files[ $j ]'.png", "'$input_files[ $j ]'.png" )' \
				-b 'pdb.gimp_quit( 1 )'
		# Prefer Krita when processing ORA files, fall back to GIMP.
		else if test ( echo "$input_files[ $j ]" | grep -iEe '\.ora$' )
			if which krita > /dev/null 2>&1
				krita "$input_files[ $j ]" --export --export-filename "$input_files[ $j ].png"
				set input_files[ $j ] "$input_files[ $j ].png"
				set using_temp_png true
			else
				gimp -i --batch-interpreter "python-fu-eval" \
					-b 'image = pdb.file_openraster_load( "'$input_files[ $j ]'", "'$input_files[ $j ]'" ); pdb.gimp_image_flatten( image ); drawable = pdb.gimp_image_get_active_drawable( image ); pdb.file_png_save_defaults( image, drawable, "'$input_files[ $j ]'.png", "'$input_files[ $j ]'.png" )' \
					-b 'pdb.gimp_quit( 1 )'
			end
		# For now, for SVG, only Inkscape is supported.
		else if test ( echo "$input_files[ $j ]" | grep -iEe '\.svgz?$' )
			inkscape "$input_files[ $j ]" -e "$input_files[ $j ].png"
		end

		if test ( echo "$input_files[ $j ]" | grep -iEe '\.(kra|xcf|xcf\.?bz2|ora|svgz?)$' )
			set input_files[ $j ] "$input_files[ $j ].png"
			set using_temp_png true
		end

		# Let the Magick happen.
		gm convert -resize "$resizes[ $k ]" -filter "Sinc" -depth "$color_depth" \
			$quality $subsampling -interlace "Line" \
			"$input_files[ $j ]" "$output_files[ $i ]"

		# Increment (or reset) our progress counters.
		set i ( math "$i + 1" )
		set k ( math "$k + 1" )
		if test $k -gt ( count $resizes )
			# Remove temporary png file.
			if test "$using_temp_png" = "true"
				trash "$input_files[ $j ]"
				set using_temp_png false
			end
			# Reset counters.
			set k 1
			set j ( math "$j + 1" )
		end
	end

	echo \n"Done!"
end
