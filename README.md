# WRIX --- Web Responsive Image Exporter

WRIX is a shell script that helps you create "responsive" images in your
website by batch exporting images to different resolutions and formats.
Generating the appropriate HTML code is up to you and your preferred CMS or
static site generator, but WRIX makes processing a set of images much easier.
For each input image, you decide the size and format and quality/compression
settings, and for each of the differently sized output image, you choose a
filename suffix to differentiate them. I like using "sd," "hd," and "uhd" to
represent the intended display resolution --- i.e., the standard definition
version would be called something like "image-sd.jpg," the high definition
version would be "image-hd.jpg," and the ultra high-def would be
"image-uhd.jpg." But you can of course use whatever suffixes you like.

I wrote this script mostly for myself, but I hope others may find it useful
too. The interactive mode especially does a pretty good of holding your hand
through each step of the process. Even people who hardly ever use the terminal
should find WRIX easy and simple.

These scripts are published under the ISC license. See the `LICENSE.md`
file, or [read it online](http://choosealicense.com/licenses/isc/).



## Setup

WRIX only works on UNIX-like systems. (Sorry, Windows users.)

WRIX is written in [fish](http://fishshell.com/), and requires fish version
2.2.0 or higher. Copy the `wrix.fish` file to your Fish functions folder
(`~/.config/fish/functions/`). You'll then be able to call `wrix` from anywhere
in the terminal.

Actual scaling of images is handled by
[GraphicsMagick](http://www.graphicsmagick.org/), so you can use
[all the image formats](http://www.graphicsmagick.org/formats.html) supported
by GraphicsMagick. If you have GIMP or Krita installed, you can also use GIMP
and Krita work files and Open Raster Archives as the input images. MyPaint
doesn't have good enough commandline support, so ORA files are initially
handled by GIMP or Krita.

[`trash-cli`](https://github.com/andreafrancia/trash-cli) is used to safely
delete temporary files (it moves them to your system trash folder), just to
make absolutely sure that your original files don't accidentally get destroyed,
even if there's a bug in this script. However you only need `trash-cli` if
using GIMP, Krita, or ORA files as the input images.



## Usage

``` fish
# Run WRIX in interactive mode.
wrix -i
```

**Other modes and features are forthcoming.** For example, I plan to add a mode
where you can choose defaults that best suit your projects and more easily
apply those. I also plan to add support for Photoshop and Manga Studio files.
For now, though, interactive mode does most of what I need, and it walks you
through the whole process.



## Notes about image formats

By default, output images are "interlaced." For JPEGs, it's usually called
"progressive" but it's the same thing. When viewing interlaced/progressive
images on a web page, the whole image loads at once, gradually increasing in
detail as the file finishes downloading. Otherwise, the image renders
vertically, in sharp detail from the beginning, but it takes longer to see the
complete image, and for large images sent over a slow connection, users spend
more time wondering what it is they're supposed to see.

I designed this script for exporting images at different sizes for the *Web*,
and even in other contexts, JPEGs, GIFs, and PNGs are the most common --- but
again, you can use any image format supported by GraphicsMagick, so if you want
to use something weird, take note:

GraphicsMagick, and therefore WRIX, supports the JPEG-2000 format, but doesn't
recognize the `j2k` file extension. Use `jp2` instead.

JNG behaves like JPEG, as far as lossy quality/compression settings in
GraphicsMagick go, but adds extra digits at the front for transparency. For a
regular JPEG, you might set the quality to 85 or so, for a nice balance of
picture quality and small file size, but the transparency has its own quality
setting --- also a number between 1 and 100. Both quality settings are written
as a single number by joining them with a zero in the middle. Transparency
quality goes first. So, if you wanted the transparency to have quality 75 and
the base image to have quality 85, you'd write 75085.

TIFF, while not exactly obscure, doesn't see a lot of use outside of print
publishing, as far as I know. Images are always Zip-compressed. You'll probably
get better compression with PNG anyway (which is also a lossless format).

As of October 2015, WebP only works in Google Chrome and the derivative web
browsers, unless you include a JavaScript shim in your web page. Look up "webp
shim" if you really want to use this image format. You can see the current
status of support for the WebP format among web browsers at
<http://caniuse.com>. Search for "webp".
